    global _main
    extern mac_exit
    extern mac_print

    section .data
message:
    db "God", 10

    section .text
_main:
    lea rdi, [rel message]
    mov rsi, 4
    call mac_print
    call mac_exit

