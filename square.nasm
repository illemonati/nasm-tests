    global square

    section .text
square:
    push rbp
    mov rbp, rsp
    mov rax, rdi
    imul rax, rax
    pop rbp
    ret
