    global _main
    default rel
    extern _putchar
    extern _printf
    extern _atoi

    section .data
star:
    db "*", 0
help_msg:
    db "pass height as arg1", 10, 0

start_str:
    db "Printing a pyromid of %d height!", 10, 0

    section .text
_main:
    mov rbp, rsp
    push rbp

    mov rax, 1
    cmp rdi, rax
    je .help

    mov rdi, qword [rsi + 8]
    call _atoi

    mov rbx, rax

    lea rdi, [start_str] 
    mov rsi, rax
    call _printf

    mov rdi, rbx
    call print_pyromid

    jmp .exit

.help:
    lea rdi, [help_msg]
    call _printf 

.exit:
    xor rax, rax
    mov rsp, rbp
    pop rbp
    ret



print_pyromid:
    push rbp
    mov rbp, rsp
    push rdi
    push qword 1

.loop:
    mov rdi, [rbp - 16]
    call print_stars
    inc qword [rbp - 16]
    mov rcx, [rbp - 16]
    cmp rcx, qword [rbp - 8]
    jle .loop

    xor rax, rax
    mov rsp, rbp
    pop rbp
    ret






print_stars:
    push rbp
    mov rbp, rsp
    push rdi
    push qword 0

.print_star_loop:
    dec qword [rbp - 8]
    lea rdi, [star]
    call _printf
    mov rcx, [rbp - 8]
    cmp rcx, qword [rbp - 16]
    jne .print_star_loop

    mov rdi, 10
    call _putchar

    xor rax, rax
    mov rsp, rbp
    pop rbp
    ret
