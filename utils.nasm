    global mac_exit
    global mac_print

    section .text
mac_exit:
    mov rax, 0x2000001
    syscall

mac_print:
    push rbp
    mov rbp, rsp
    mov rdx, rsi
    mov rsi, rdi
    mov rax, 0x2000004
    mov rdi, 1
    syscall
    pop rbp
    ret


