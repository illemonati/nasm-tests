    global _main
    extern square
    extern _printf
    default rel

    section .data
number_format:
    db "Sqaure of %d = %d", 10, 0

number_to_square:
    dq 24
    
    section .text
_main:
    push rbp
    mov rdi, [number_to_square]
    call square
    lea rdi, [number_format]
    mov rsi, [number_to_square]
    mov rdx, rax
    xor rax, rax
    call _printf
    pop rbp
    mov rax, 0
    ret
