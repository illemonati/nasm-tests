


section .data
start: db "Start!", 10, 0
num_format: db "Number is %d", 10, 0
num: dq 10


section .bss


section .text
extern _printf
default rel
global _main
global _counting_loop

_main:
    push rbp
    mov rbp, rsp

    mov rdi, start
    mov rax, 0
    call _printf

    mov rdi, [num]
    call counting_loop

    mov rsp, rbp
    pop rbp
    ret

counting_loop:
    push rbp
    mov rbp, rsp

    push qword 0
    push rdi

.l:
    lea rdi, [num_format]
    mov rsi, [rbp - 16]
    call _printf
    dec qword [rbp - 16]

    mov rcx, [rbp-16]
    cmp rcx, [rbp-8]
    jne .l


    xor rax, rax
    mov rsp, rbp
    pop rbp
    ret
